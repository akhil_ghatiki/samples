package com.example.aghatiki.samplechart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
/**
 * Created by aghatiki on 8/25/2016.
 */
public class MyAxisValueFormatter implements AxisValueFormatter {
    private String[] mValues;
    public MyAxisValueFormatter(String[] values)
    {
        this.mValues = values;
    }
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        return mValues[(int) value];
    }

    @Override
    public int getDecimalDigits() {
        return 0;
    }
}
